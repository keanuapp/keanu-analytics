from os import path

from setuptools import find_packages
from setuptools import setup

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    install_requires = f.read().splitlines()

setup(
    name="keanu-analytics",
    version="0.1",
    packages=find_packages(exclude=['docs']),
    install_requires=install_requires,
    author="Iain R. Learmonth",
    author_email="iain@learmonth.me",
    description=("This is an Application Service providing privacy-preserving "
                 "metrics for a Synapse server"),
    long_description=long_description,
    keywords="matrix synapse privacy metrics analytics",
    url="https://gitlab.com/keanuapp/keanu-analytics",
    project_urls={
        "Bug Tracker": "https://gitlab.com/keanuapp/keanu-analytics/-/issues",
        "Source Code": "https://gitlab.com/keanuapp/keanu-analytics",
    },
    classifiers=["License :: OSI Approved :: BSD License"],
    entry_points={
        'console_scripts': [
            'kealytics=kealytics.app_service:run_server',
        ],
    },
    test_suite='nose.collector',
)

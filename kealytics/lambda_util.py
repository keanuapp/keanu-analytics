import json
import os.path
import tempfile
from typing import Dict
from typing import List

import boto3
import psycopg2

def ssm_parameter(name: str) -> str:
    ssm = boto3.client('ssm')
    result = ssm.get_parameter(Name=name, WithDecryption=True)
    return result['Parameter']['Value']


def db_connect():
    """
    Returns a connection to the PostgreSQL database.
    """
    config = {
        'host': ssm_parameter("/analytics/db/host"),
        'user': ssm_parameter("/analytics/db/user"),
        'password': ssm_parameter("/analytics/db/pass"),
        'dbname': ssm_parameter("/analytics/db/name"),
        'port': ssm_parameter("/analytics/db/port"),
    }
    print(config)
    return psycopg2.connect(**config)


def geoip_db() -> str:
    """
    Downloads the GeoIP database from S3 and returns a local filename.
    """
    s3 = boto3.client('s3')
    geoip_bucket = ssm_parameter("/analytics/config/bucket")
    geoip_file = tempfile.NamedTemporaryFile(delete=False)
    s3.download_fileobj(geoip_bucket, "GeoIP2-City.mmdb", geoip_file)
    return geoip_file.name


def region_config() -> List[Dict[str, str]]:
    s3 = boto3.client('s3')
    geojson_bucket = ssm_parameter("/analytics/config/bucket")
    geojson_folder = tempfile.mkdtemp()
    s3.download_file(geojson_bucket, "regions.json", os.path.join(geojson_folder, "regions.json"))
    with open(os.path.join(geojson_folder, "regions.json")) as config_json:
        configs = json.load(config_json)
    regions = []
    for config in configs:
        filename = os.path.join(geojson_folder, config['path'])
        s3.download_file(geojson_bucket, config['path'], filename)
        regions.append({'name': config['name'], 'path': filename})
    return regions

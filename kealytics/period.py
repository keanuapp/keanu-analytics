import time
from datetime import datetime
from datetime import timezone
from typing import Optional

import isoweek


def period_for_timestamp(ts_msec: Optional[int]) -> str:
    """Return the period identifier that a timestamp falls within.

    To get the current period, use None for ts.

    :param ts: Timestamp in milliseconds since midnight January 1st 1970.
    """
    ts_sec: float = time.time() if ts_msec is None else ts_msec / 1000
    date = datetime.fromtimestamp(ts_sec, timezone.utc)
    week = isoweek.Week.withdate(date)
    return f"{week.year}-W{week.week}"
